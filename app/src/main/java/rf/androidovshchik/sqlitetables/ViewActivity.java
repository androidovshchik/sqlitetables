package rf.androidovshchik.sqlitetables;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.squareup.sqlbrite3.SqlBrite;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.base.BaseActivity;
import rf.androidovshchik.sqlitetables.models.FailRow;

public class ViewActivity extends BaseActivity {

    public static final String EXTRA_STATION_ID = "stationId";
    public static final String EXTRA_SHOW_ADD = "showAdd";
    public static final String EXTRA_DISPLAY_ID = "displayId";

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.view_add)
    View add;

    private FailAdapter adapter;

    @Override
    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        ButterKnife.bind(this);
        if (getIntent().hasExtra(EXTRA_STATION_ID)) {
            title.setText("Неисправности радиостанции " + getIntent().getLongExtra(EXTRA_STATION_ID, 0));
        } else {
            title.setText("Последние неисправности");
        }
        add.setVisibility(getIntent().getBooleanExtra(EXTRA_SHOW_ADD, false) ?
            View.VISIBLE : View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new FailAdapter(getIntent().getBooleanExtra(EXTRA_DISPLAY_ID, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        String sql = "SELECT fail.*, object.model AS model_object FROM fail";
        sql += " LEFT JOIN object ON fail.nomer = object.nomer";
        if (getIntent().hasExtra(EXTRA_STATION_ID)) {
            sql += " WHERE fail.id_radiostation = " + getIntent().getLongExtra(EXTRA_STATION_ID, 0);
        }
        sql += " ORDER BY fail.rowid DESC";
        Observable<SqlBrite.Query> savedExpenses = manager.db.createQuery(FailRow.TABLE, sql);
        disposable.add(savedExpenses.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .firstElement()
            .subscribe((SqlBrite.Query query) -> {
                Cursor cursor = handleCursor(query);
                if (cursor == null) {
                    return;
                }
                adapter.items.clear();
                try {
                    while (cursor.moveToNext()) {
                        FailRow failRow = new FailRow();
                        failRow.parseCursor(cursor);
                        adapter.items.add(failRow);
                    }
                } finally {
                    cursor.close();
                }
                adapter.notifyDataSetChanged();
            }));
    }

    @OnClick(R.id.view_add)
    void onAdd() {
        startActivity(AddFailActivity.class);
    }

    @OnClick(R.id.view_back)
    void onBack() {
        finish();
    }
}
