package rf.androidovshchik.sqlitetables;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import rf.androidovshchik.sqlitetables.base.BaseActivity;

public class MenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.menu_button1)
    void onButton1() {
        startActivity(AddObjectActivity.class);
    }

    @OnClick(R.id.menu_button2)
    void onButton2() {
        startActivity(AddStationActivity.class);
    }

    @OnClick(R.id.menu_button3)
    void onButton3() {
        startActivity(SearchObjectActivity.class);
    }

    @OnClick(R.id.menu_button4)
    void onButton4() {
        startActivity(SearchStationActivity.class);
    }

    @OnClick(R.id.menu_button5)
    void onButton5() {
        Intent intent = new Intent(getApplicationContext(), ViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ViewActivity.EXTRA_SHOW_ADD, true);
        intent.putExtra(ViewActivity.EXTRA_DISPLAY_ID, true);
        startActivity(intent);
    }
}
