package rf.androidovshchik.sqlitetables.views;

import android.content.Context;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EditLayout extends LinearLayout {

    public EditText editText;

    public EditLayout(Context context, String caption, int editId, boolean inputNumber, boolean disabled) {
        super(context);
        setOrientation(HORIZONTAL);
        setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        ));
        TextView textView = new TextView(getApplicationContext());
        textView.setText(caption);
        textView.setTextSize(16);
        LayoutParams textViewParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );
        addView(textView, textViewParams);
        editText = new EditText(getApplicationContext());
        if (editId != 0) {
            editText.setId(editId);
        }
        editText.setEnabled(!disabled);
        if (inputNumber) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL |
                    InputType.TYPE_NUMBER_FLAG_SIGNED);
            editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        } else {
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        editText.setMaxLines(1);
        LayoutParams editTextParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );
        editTextParams.weight = 1;
        addView(editText, editTextParams);
    }

    private Context getApplicationContext() {
        return getContext().getApplicationContext();
    }
}
