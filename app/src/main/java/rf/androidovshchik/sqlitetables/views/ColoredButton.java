package rf.androidovshchik.sqlitetables.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

public class ColoredButton extends Button {

    public ColoredButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getBackground().setColorFilter(0xFF2196F3, PorterDuff.Mode.MULTIPLY);
        } else {
            getBackground().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
        }
    }
}
