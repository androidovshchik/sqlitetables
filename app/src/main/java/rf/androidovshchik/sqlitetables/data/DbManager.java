package rf.androidovshchik.sqlitetables.data;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Factory;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;

import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.BuildConfig;
import rf.androidovshchik.sqlitetables.models.Row;

public class DbManager {

    public BriteDatabase db;

    public DbManager(Context context) {
        DbCallback dbCallback = new DbCallback();
        dbCallback.openDatabase(context);
        Configuration configuration = Configuration.builder(context)
            .name(DbCallback.DATABASE_NAME)
            .callback(dbCallback)
            .build();
        Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper openHelper = factory.create(configuration);
        db = new SqlBrite.Builder()
            .logger(message -> Log.v("Db", message))
            .build()
            .wrapDatabaseHelper(openHelper, Schedulers.io());
        db.setLoggingEnabled(BuildConfig.DEBUG);
    }

    public long insertRow(Row row) {
        return db.insert(row.getTable(), SQLiteDatabase.CONFLICT_REPLACE, row.toContentValues());
    }

    @SuppressWarnings("all")
    public int deleteTable(String table) {
        return db.delete(table, null, null);
    }
}
