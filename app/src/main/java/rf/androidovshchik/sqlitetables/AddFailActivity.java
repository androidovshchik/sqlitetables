package rf.androidovshchik.sqlitetables;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.squareup.sqlbrite3.BriteDatabase;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.base.BaseActivity;
import rf.androidovshchik.sqlitetables.models.FailRow;
import rf.androidovshchik.sqlitetables.views.EditLayout;
import timber.log.Timber;

public class AddFailActivity extends BaseActivity {

    private DatePicker date;
    private EditLayout vid;
    private EditLayout nomer;
    private EditLayout id_radiostation;
    private EditLayout model_radio;
    private EditLayout vid_fail;
    private EditLayout prichina_fail;
    private EditLayout ystraneno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        container = findViewById(R.id.container);
        findViewById(R.id.contentLayout).requestFocus();
        date = new DatePicker(getApplicationContext());
        LinearLayout.LayoutParams datePickerParams = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        );
        datePickerParams.gravity = Gravity.CENTER_HORIZONTAL;
        date.setLayoutParams(datePickerParams);
        container.addView(date);
        vid = addEditLayout("Вид объекта");
        nomer = addNumberEditLayout("Номер объекта");
        id_radiostation = addNumberEditLayout("Id_Радиостанции");
        model_radio = addEditLayout("Модель р/cт");
        vid_fail = addEditLayout("Вид");
        prichina_fail = addEditLayout("Причина");
        ystraneno = addEditLayout("Устранено");
        ButterKnife.bind(this);
        title.setText("Добавить неисправность");
    }

    @SuppressWarnings("all")
    @OnClick(R.id.edit_add)
    void onEditAdd() {
        disposable.add(Observable.fromCallable(() -> {
            BriteDatabase.Transaction transaction = manager.db.newTransaction();
            try {
                FailRow failRow = new FailRow();
                failRow.date = FailRow.SIMPLE_DATE_FORMAT.format(getDate(date));
                failRow.vid = vid.editText.getText().toString();
                failRow.nomer = nomer.editText.getText().toString();
                try {
                    failRow.id_radiostation = Long.parseLong(id_radiostation.editText.getText().toString());
                } catch (NumberFormatException e) {
                    failRow.id_radiostation = null;
                }
                failRow.model_radio = model_radio.editText.getText().toString();
                failRow.vid_fail = vid_fail.editText.getText().toString();
                failRow.prichina_fail = prichina_fail.editText.getText().toString();
                failRow.ystraneno = ystraneno.editText.getText().toString();
                Timber.d(failRow.toString());
                manager.insertRow(failRow);
                transaction.markSuccessful();
            } finally {
                transaction.close();
            }
            return true;
        }).subscribeOn(Schedulers.io())
                .subscribe((Boolean value) -> {
                    finish();
                }));
    }

    @OnClick(R.id.edit_back)
    void onEditBack() {
        finish();
    }
}
