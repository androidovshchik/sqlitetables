package rf.androidovshchik.sqlitetables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.squareup.sqlbrite3.BriteDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.base.BaseActivity;
import rf.androidovshchik.sqlitetables.models.ObjectRow;
import rf.androidovshchik.sqlitetables.models.StationObject;
import rf.androidovshchik.sqlitetables.views.EditLayout;

public class SearchObjectActivity extends BaseActivity {

    public static final int ID_VID = 0x2000;
    public static final int ID_NOMER = 0x3000;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private EditLayout vid;
    private EditLayout nomer;

    private EditLayout model;
    private EditLayout mesto;
    private EditLayout u;

    private StationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_object);
        container = findViewById(R.id.container);
        findViewById(R.id.contentLayout).requestFocus();
        findViewById(R.id.edit_add).setVisibility(View.GONE);
        vid = addEditLayout("Вид объекта", ID_VID, false, false);
        nomer = addEditLayout("Номер объекта", ID_NOMER, true, false);
        model = addDisabledEditLayout("Модель");
        mesto = addDisabledEditLayout("Местоположение");
        u = addDisabledEditLayout("U Пит. Сети");
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new StationAdapter();
        recyclerView.setAdapter(adapter);
        title.setText("Поиск по объекту");
        onInput();
    }

    @SuppressWarnings("all")
    @OnTextChanged(value = {ID_VID, ID_NOMER}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onInput() {
        disposable.clear();
        model.editText.setText("");
        mesto.editText.setText("");
        u.editText.setText("");
        adapter.items.clear();
        adapter.notifyDataSetChanged();
        disposable.add(Observable.fromCallable(() -> {
            ObjectRow objectRow = null;
            BriteDatabase.Transaction transaction = manager.db.newTransaction();
            try {
                String vid = this.vid.editText.getText().toString().trim();
                String nomer = DatabaseUtils.sqlEscapeString(this.nomer.editText.getText().toString().trim());
                if (!vid.isEmpty()) {
                    Cursor cursor = manager.db.query("SELECT * FROM object WHERE LOWER(vid) LIKE LOWER('%" + vid +
                        "%') AND nomer = " + nomer);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                objectRow = new ObjectRow();
                                objectRow.parseCursor(cursor);
                            }
                        } finally {
                            cursor.close();
                        }
                    }
                    adapter.items.addAll(getRows(manager.db.query("SELECT * FROM radio_station WHERE" +
                            " LOWER(vid) LIKE LOWER('%" + vid + "%') AND nomer = " + nomer),
                        StationObject.class));
                }
                transaction.markSuccessful();
            } finally {
                transaction.close();
            }
            return objectRow;
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((ObjectRow objectRow) -> {
                model.editText.setText(objectRow.model);
                mesto.editText.setText(objectRow.mesto);
                u.editText.setText(String.valueOf(objectRow.u));
                adapter.notifyDataSetChanged();
            }, (Throwable throwable) -> adapter.notifyDataSetChanged()));
    }

    @OnClick(R.id.edit_back)
    void onEditBack() {
        finish();
    }
}
