package rf.androidovshchik.sqlitetables;

import android.app.Application;

import timber.log.Timber;

public class SqliteTables extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		if (BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
		}
	}
}