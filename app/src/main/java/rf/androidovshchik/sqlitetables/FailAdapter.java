package rf.androidovshchik.sqlitetables;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import rf.androidovshchik.sqlitetables.base.BaseAdapter;
import rf.androidovshchik.sqlitetables.base.BaseViewHolder;
import rf.androidovshchik.sqlitetables.models.FailRow;

public class FailAdapter extends BaseAdapter<FailRow, FailAdapter.ViewHolder> {

    private boolean displayId;

    public FailAdapter(boolean displayId) {
        super();
        this.displayId = displayId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,
            parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    @SuppressWarnings("all")
    public void onBindViewHolder(ViewHolder holder, int position) {
        FailRow failRow = items.get(position);
        StringBuilder builder = new StringBuilder();
        builder.append((position + 1) + ". ");
        builder.append(failRow.date + "; ");
        builder.append(failRow.vid + "; ");
        if (displayId) {
            builder.append(failRow.id_radiostation + "; ");
        }
        builder.append(failRow.model_object + "; ");
        builder.append(failRow.model_radio + "; ");
        builder.append(failRow.vid_fail + "; ");
        builder.append(failRow.prichina_fail + "; ");
        builder.append(failRow.ystraneno + ".");
        holder.name.setText(builder.toString());
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.name)
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
