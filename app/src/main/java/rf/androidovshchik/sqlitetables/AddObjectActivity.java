package rf.androidovshchik.sqlitetables;

import android.os.Bundle;

import com.squareup.sqlbrite3.BriteDatabase;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.base.BaseActivity;
import rf.androidovshchik.sqlitetables.models.ObjectRow;
import rf.androidovshchik.sqlitetables.views.EditLayout;
import timber.log.Timber;

public class AddObjectActivity extends BaseActivity {

    private EditLayout vid;
    private EditLayout nomer;
    private EditLayout model;
    private EditLayout mesto;
    private EditLayout u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        container = findViewById(R.id.container);
        findViewById(R.id.contentLayout).requestFocus();
        vid = addEditLayout("Вид объекта");
        nomer = addNumberEditLayout("Номер объекта");
        model = addEditLayout("Модель");
        mesto = addEditLayout("Местоположение");
        u = addNumberEditLayout("U Пит. Сети");
        ButterKnife.bind(this);
        title.setText("Добавить объект");
    }

    @SuppressWarnings("all")
    @OnClick(R.id.edit_add)
    void onEditAdd() {
        disposable.add(Observable.fromCallable(() -> {
            BriteDatabase.Transaction transaction = manager.db.newTransaction();
            try {
                ObjectRow objectRow = new ObjectRow();
                objectRow.vid = vid.editText.getText().toString();
                objectRow.nomer = nomer.editText.getText().toString();
                objectRow.model = model.editText.getText().toString();
                objectRow.mesto = mesto.editText.getText().toString();
                try {
                    objectRow.u = Integer.parseInt(u.editText.getText().toString());
                } catch (NumberFormatException e) {
                    objectRow.u = null;
                }
                Timber.d(objectRow.toString());
                manager.insertRow(objectRow);
                transaction.markSuccessful();
            } finally {
                transaction.close();
            }
            return true;
        }).subscribeOn(Schedulers.io())
                .subscribe((Boolean value) -> {
                    finish();
                }));
    }

    @OnClick(R.id.edit_back)
    void onEditBack() {
        finish();
    }
}
