package rf.androidovshchik.sqlitetables;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rf.androidovshchik.sqlitetables.base.BaseAdapter;
import rf.androidovshchik.sqlitetables.base.BaseViewHolder;
import rf.androidovshchik.sqlitetables.models.StationObject;
import rf.androidovshchik.sqlitetables.views.EditLayout;

public class StationAdapter extends BaseAdapter<StationObject, StationAdapter.ViewHolder> {

    public static final int ID_CONTAINER = 0x4000;
    public static final int ID_STATION = 0x5000;
    public static final int ID_MODEL = 0x6000;
    public static final int ID_MESTO = 0x7000;

    public StationAdapter() {
        super();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext().getApplicationContext();
        LinearLayout itemView = new LinearLayout(context);
        itemView.setId(ID_CONTAINER);
        itemView.setLayoutParams(new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        itemView.setOrientation(LinearLayout.VERTICAL);
        EditLayout id_radiostation = new EditLayout(context, "Id р/ст", 0, true, true);
        id_radiostation.setId(ID_STATION);
        itemView.addView(id_radiostation);
        EditLayout model_radio = new EditLayout(context, "Модель р/ст", 0, false, true);
        model_radio.setId(ID_MODEL);
        itemView.addView(model_radio);
        EditLayout mesto_radio = new EditLayout(context, "Место установки р/ст", 0, false, true);
        mesto_radio.setId(ID_MESTO);
        itemView.addView(mesto_radio);
        return new ViewHolder(itemView);
    }

    @Override
    @SuppressWarnings("all")
    public void onBindViewHolder(ViewHolder holder, int position) {
        StationObject stationObject = items.get(position);
        holder.id_radiostation.editText.setText(String.valueOf(stationObject.id_radiostation));
        holder.model_radio.editText.setText(stationObject.model_radio);
        holder.mesto_radio.editText.setText(stationObject.mesto_radio);
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(ID_STATION)
        public EditLayout id_radiostation;
        @BindView(ID_MODEL)
        public EditLayout model_radio;
        @BindView(ID_MESTO)
        public EditLayout mesto_radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(ID_CONTAINER)
        void onStation() {
            Intent intent = new Intent(getApplicationContext(), SearchStationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                intent.putExtra(SearchStationActivity.EXTRA_STATION_ID,
                    Long.parseLong(id_radiostation.editText.getText().toString()));
            } catch (NumberFormatException e) {
                intent.putExtra(SearchStationActivity.EXTRA_STATION_ID, 0L);
            }
            getApplicationContext().startActivity(intent);
        }
    }
}
