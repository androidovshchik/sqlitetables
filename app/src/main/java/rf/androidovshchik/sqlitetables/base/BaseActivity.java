package rf.androidovshchik.sqlitetables.base;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.sqlbrite3.SqlBrite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.sqlitetables.R;
import rf.androidovshchik.sqlitetables.data.DbManager;
import rf.androidovshchik.sqlitetables.models.Row;
import rf.androidovshchik.sqlitetables.views.EditLayout;
import timber.log.Timber;

public class BaseActivity extends Activity {

    @BindView(R.id.title)
    protected TextView title;

    @Nullable
    protected LinearLayout container;

    protected DbManager manager;

    protected CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = new DbManager(getApplicationContext());
    }

    protected void startActivity(Class activity) {
        Intent intent = new Intent(getApplicationContext(), activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected EditLayout addEditLayout(String caption) {
        return addEditLayout(caption, 0, false, false);
    }

    protected EditLayout addNumberEditLayout(String caption) {
        return addEditLayout(caption, 0, true, false);
    }

    protected EditLayout addDisabledEditLayout(String caption) {
        return addEditLayout(caption, 0, false, true);
    }

    protected EditLayout addEditLayout(String caption, int resId, boolean inputNumber, boolean disabled) {
        EditLayout editLayout = new EditLayout(getApplicationContext(), caption, resId,
            inputNumber, disabled);
        if (container == null) {
            return editLayout;
        }
        container.addView(editLayout);
        return editLayout;
    }

    public static Date getDate(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

    @Nullable
    public static Cursor handleCursor(@Nullable SqlBrite.Query query) {
        if (query == null) {
            Timber.w("Query is null");
            return null;
        }
        Cursor cursor = query.run();
        if (cursor == null) {
            Timber.w("Cursor is null");
            return null;
        }
        return cursor;
    }

    protected  <T extends Row> ArrayList<T> getRows(Cursor cursor, Class<T> rowClass) throws Exception {
        ArrayList<T> rows = new ArrayList<>();
        if (cursor == null) {
            return rows;
        }
        try {
            while (cursor.moveToNext()) {
                T row = rowClass.newInstance();
                row.parseCursor(cursor);
                rows.add(row);
            }
        } finally {
            cursor.close();
        }
        return rows;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
