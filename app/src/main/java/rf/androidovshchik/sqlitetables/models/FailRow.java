package rf.androidovshchik.sqlitetables.models;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.SimpleDateFormat;

public class FailRow extends Row {

	@SuppressWarnings("all")
	public static final SimpleDateFormat SIMPLE_DATE_FORMAT =
		new SimpleDateFormat("yyyy-MM-dd");

	public static final String TABLE = "fail";

	public static final String COLUMN_ID_RADIOSTATION = "id_radiostation";
	// join field
	public static final String COLUMN_MODEL_OBJECT = "model_object";
	public static final String COLUMN_MODEL_RADIO = "Model_radio";
	public static final String COLUMN_VID = "vid";
	public static final String COLUMN_NOMER = "nomer";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_VID_FAIL = "vid_fail";
	public static final String COLUMN_PROCHINA_FAIL = "prichina_fail";
	public static final String COLUMN_YSTRANENO = "ystraneno";

	public Long id_radiostation;

	// join field
	public String model_object;

	public String model_radio;

	public String vid;

	public String nomer;

	public String date;

	public String vid_fail;

	public String prichina_fail;

	public String ystraneno;

	public FailRow() {}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(COLUMN_ID_RADIOSTATION, id_radiostation);
		values.put(COLUMN_MODEL_RADIO, model_radio);
		values.put(COLUMN_VID, vid);
		values.put(COLUMN_NOMER, nomer);
		values.put(COLUMN_DATE, date);
		values.put(COLUMN_VID_FAIL, vid_fail);
		values.put(COLUMN_PROCHINA_FAIL, prichina_fail);
		values.put(COLUMN_YSTRANENO, ystraneno);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id_radiostation = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID_RADIOSTATION));
		try {
			model_object = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MODEL_OBJECT));
		} catch (IllegalArgumentException e) {
			model_object = null;
		}
		model_radio = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MODEL_RADIO));
		vid = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_VID));
		nomer = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NOMER));
		date = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATE));
		vid_fail = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_VID_FAIL));
		prichina_fail = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PROCHINA_FAIL));
		ystraneno = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_YSTRANENO));
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "FailRow{" +
			"id_radiostation=" + id_radiostation +
			", model_object='" + model_object + '\'' +
			", model_radio='" + model_radio + '\'' +
			", vid='" + vid + '\'' +
			", nomer='" + nomer + '\'' +
			", date='" + date + '\'' +
			", vid_fail='" + vid_fail + '\'' +
			", prichina_fail='" + prichina_fail + '\'' +
			", ystraneno='" + ystraneno + '\'' +
			'}';
	}
}
