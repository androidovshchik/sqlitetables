package rf.androidovshchik.sqlitetables.models;

import android.content.ContentValues;
import android.database.Cursor;

public class ObjectRow extends Row {

	public static final String TABLE = "object";

	public static final String COLUMN_VID = "vid";
	public static final String COLUMN_NOMER = "nomer";
	public static final String COLUMN_MODEL = "model";
	public static final String COLUMN_MESTO = "mesto";
	public static final String COLUMN_U = "U";

	public String vid;

	public String nomer;

	public String model;

	public String mesto;

	public Integer u;

	public ObjectRow() {}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(COLUMN_VID, vid);
		values.put(COLUMN_NOMER, nomer);
		values.put(COLUMN_MODEL, model);
		values.put(COLUMN_MESTO, mesto);
		values.put(COLUMN_U, u);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		vid = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_VID));
		nomer = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NOMER));
		model = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MODEL));
		mesto = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MESTO));
		u = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_U));
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "ObjectRow{" +
				"vid='" + vid + '\'' +
				", nomer='" + nomer + '\'' +
				", model='" + model + '\'' +
				", mesto='" + mesto + '\'' +
				", u=" + u +
				'}';
	}
}
