package rf.androidovshchik.sqlitetables.models;

import android.content.ContentValues;
import android.database.Cursor;

public class StationObject extends Row {

	public static final String TABLE = "radio_station";

	public static final String COLUMN_ID_RADIOSTATION = "id_radiostation";
	public static final String COLUMN_VID = "vid";
	public static final String COLUMN_NOMER = "nomer";
	public static final String COLUMN_MODEL_RADIO = "model_radio";
	public static final String COLUMN_POZIVNOI = "pozivnoi";
	public static final String COLUMN_BLOCK_PIT = "Bloc_pit";
	public static final String COLUMN_ANTENS = "antens";
	public static final String COLUMN_KABEL = "kabel";
	public static final String COLUMN_MESTO_RADIO = "mesto_radio";
	public static final String COLUMN_MESTO_ANTEN = "mesto_anten";

	public Long id_radiostation;

	public String vid;

	public String nomer;

	public String model_radio;

	public String pozivnoi;

	public String bloc_pit;

	public String antens;

	public String kabel;

	public String mesto_radio;

	public String mesto_anten;

	public StationObject() {}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (id_radiostation != 0L) {
			values.put(COLUMN_ID_RADIOSTATION, id_radiostation);
		}
		values.put(COLUMN_VID, vid);
		values.put(COLUMN_NOMER, nomer);
		values.put(COLUMN_MODEL_RADIO, model_radio);
		values.put(COLUMN_POZIVNOI, pozivnoi);
		values.put(COLUMN_BLOCK_PIT, bloc_pit);
		values.put(COLUMN_ANTENS, antens);
		values.put(COLUMN_KABEL, kabel);
		values.put(COLUMN_MESTO_RADIO, mesto_radio);
		values.put(COLUMN_MESTO_ANTEN, mesto_anten);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id_radiostation = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID_RADIOSTATION));
		vid = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_VID));
		nomer = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NOMER));
		model_radio = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MODEL_RADIO));
		pozivnoi = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_POZIVNOI));
		bloc_pit = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BLOCK_PIT));
		antens = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ANTENS));
		kabel = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_KABEL));
		mesto_radio = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MESTO_RADIO));
		mesto_anten = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MESTO_ANTEN));
	}

	@Override
	public String getTable() {
		return TABLE;
	}

	@Override
	public String toString() {
		return "StationObject{" +
				"id_radiostation=" + id_radiostation +
				", vid='" + vid + '\'' +
				", nomer='" + nomer + '\'' +
				", model_radio='" + model_radio + '\'' +
				", pozivnoi='" + pozivnoi + '\'' +
				", bloc_pit='" + bloc_pit + '\'' +
				", antens='" + antens + '\'' +
				", kabel='" + kabel + '\'' +
				", mesto_radio='" + mesto_radio + '\'' +
				", mesto_anten='" + mesto_anten + '\'' +
				'}';
	}
}
