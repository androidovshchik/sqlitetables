package rf.androidovshchik.sqlitetables;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

import com.squareup.sqlbrite3.SqlBrite;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.base.BaseActivity;
import rf.androidovshchik.sqlitetables.models.StationObject;
import rf.androidovshchik.sqlitetables.views.EditLayout;

public class SearchStationActivity extends BaseActivity {

    public static final String EXTRA_STATION_ID = "stationId";

    public static final int ID_RADIO_STATION = 0x1000;

    private EditLayout id_radiostation;

    private EditLayout vid;
    private EditLayout nomer;
    private EditLayout model_radio;
    private EditLayout pozivnoi;
    private EditLayout bloc_pit;
    private EditLayout antens;
    private EditLayout kabel;
    private EditLayout mesto_radio;
    private EditLayout mesto_anten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        container = findViewById(R.id.container);
        findViewById(R.id.contentLayout).requestFocus();
        findViewById(R.id.edit_add).setVisibility(View.GONE);
        findViewById(R.id.edit_fail).setVisibility(View.VISIBLE);
        id_radiostation = addEditLayout("Id р/ст", ID_RADIO_STATION, true, false);
        if (getIntent().hasExtra(EXTRA_STATION_ID)) {
            id_radiostation.editText.setText(String.valueOf(getIntent().getLongExtra(EXTRA_STATION_ID, 0L)));
        }
        vid = addDisabledEditLayout("Вид объекта");
        nomer = addDisabledEditLayout("Номер объекта");
        model_radio = addDisabledEditLayout("Модель р/ст");
        pozivnoi = addDisabledEditLayout("Позывной");
        bloc_pit = addDisabledEditLayout("Блок питания");
        antens = addDisabledEditLayout("Антена");
        kabel = addDisabledEditLayout("Кабель");
        mesto_radio = addDisabledEditLayout("Место установки р/ст");
        mesto_anten = addDisabledEditLayout("Место установки антены");
        ButterKnife.bind(this);
        title.setText("Поиск радиостанции");
        onInput();
    }

    @OnTextChanged(value = {ID_RADIO_STATION}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onInput() {
        disposable.clear();
        vid.editText.setText("");
        nomer.editText.setText("");
        model_radio.editText.setText("");
        pozivnoi.editText.setText("");
        bloc_pit.editText.setText("");
        antens.editText.setText("");
        kabel.editText.setText("");
        mesto_radio.editText.setText("");
        mesto_anten.editText.setText("");
        long stationId;
        try {
            stationId = Long.parseLong(id_radiostation.editText.getText().toString());
        } catch (NumberFormatException e) {
            stationId = 0L;
        }
        String sql = "SELECT * FROM radio_station WHERE id_radiostation = " + stationId;
        Observable<SqlBrite.Query> savedExpenses = manager.db.createQuery(StationObject.TABLE, sql);
        disposable.add(savedExpenses.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .firstElement()
            .subscribe((SqlBrite.Query query) -> {
                Cursor cursor = handleCursor(query);
                if (cursor == null) {
                    return;
                }
                try {
                    if (cursor.moveToFirst()) {
                        StationObject stationObject = new StationObject();
                        stationObject.parseCursor(cursor);
                        vid.editText.setText(stationObject.vid);
                        nomer.editText.setText(stationObject.nomer);
                        model_radio.editText.setText(stationObject.model_radio);
                        pozivnoi.editText.setText(stationObject.pozivnoi);
                        bloc_pit.editText.setText(stationObject.bloc_pit);
                        antens.editText.setText(stationObject.antens);
                        kabel.editText.setText(stationObject.kabel);
                        mesto_radio.editText.setText(stationObject.mesto_radio);
                        mesto_anten.editText.setText(stationObject.mesto_anten);
                    }
                } finally {
                    cursor.close();
                }
            }));
    }

    @OnClick(R.id.edit_back)
    void onEditBack() {
        finish();
    }

    @OnClick(R.id.edit_fail)
    void onEditFail() {
        Intent intent = new Intent(getApplicationContext(), ViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ViewActivity.EXTRA_SHOW_ADD, false);
        intent.putExtra(ViewActivity.EXTRA_DISPLAY_ID, false);
        try {
            intent.putExtra(ViewActivity.EXTRA_STATION_ID,
                Long.parseLong(id_radiostation.editText.getText().toString()));
        } catch (NumberFormatException e) {
            intent.putExtra(ViewActivity.EXTRA_STATION_ID, 0L);
        }
        startActivity(intent);
    }
}
