package rf.androidovshchik.sqlitetables;

import android.os.Bundle;

import com.squareup.sqlbrite3.BriteDatabase;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.sqlitetables.base.BaseActivity;
import rf.androidovshchik.sqlitetables.models.StationObject;
import rf.androidovshchik.sqlitetables.views.EditLayout;
import timber.log.Timber;

public class AddStationActivity extends BaseActivity {

    private EditLayout vid;
    private EditLayout nomer;
    private EditLayout model_radio;
    private EditLayout pozivnoi;
    private EditLayout bloc_pit;
    private EditLayout antens;
    private EditLayout kabel;
    private EditLayout mesto_radio;
    private EditLayout mesto_anten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        container = findViewById(R.id.container);
        findViewById(R.id.contentLayout).requestFocus();
        vid = addEditLayout("Вид объекта");
        nomer = addNumberEditLayout("Номер объекта");
        model_radio = addEditLayout("Модель р/ст");
        pozivnoi = addEditLayout("Позывной");
        bloc_pit = addEditLayout("Блок питания");
        antens = addEditLayout("Антена");
        kabel = addEditLayout("Кабель");
        mesto_radio = addEditLayout("Место установки р/ст");
        mesto_anten = addEditLayout("Место установки антены");
        ButterKnife.bind(this);
        title.setText("Добавить радиостанцию");
    }

    @SuppressWarnings("all")
    @OnClick(R.id.edit_add)
    void onEditAdd() {
        disposable.add(Observable.fromCallable(() -> {
            BriteDatabase.Transaction transaction = manager.db.newTransaction();
            try {
                StationObject stationObject = new StationObject();
                stationObject.id_radiostation = 0L;
                stationObject.vid = vid.editText.getText().toString();
                stationObject.nomer = nomer.editText.getText().toString();
                stationObject.model_radio = model_radio.editText.getText().toString();
                stationObject.pozivnoi = pozivnoi.editText.getText().toString();
                stationObject.bloc_pit = bloc_pit.editText.getText().toString();
                stationObject.antens = antens.editText.getText().toString();
                stationObject.kabel = kabel.editText.getText().toString();
                stationObject.mesto_radio = mesto_radio.editText.getText().toString();
                stationObject.mesto_anten = mesto_anten.editText.getText().toString();
                Timber.d(stationObject.toString());
                manager.insertRow(stationObject);
                transaction.markSuccessful();
            } finally {
                transaction.close();
            }
            return true;
        }).subscribeOn(Schedulers.io())
                .subscribe((Boolean value) -> {
                    finish();
                }));
    }

    @OnClick(R.id.edit_back)
    void onEditBack() {
        finish();
    }
}
